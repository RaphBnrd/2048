
import numpy as np
import random as rd
import tkinter as tk


class Game(tk.Tk):

    def __init__(self, size_grid=4, proba_4=0.15):
        tk.Tk.__init__(self)
        self.grid = Grid(size_grid, proba_4)
        self.status = "En cours"
        self.colors = {'0':'#b5b5b5', '2':'#ffffff', 
                       '4':'#fffff1', '8':'#fefeeb', '16':'#fefee4', '32':'#fdfdca', 
                       '64':'#fcfcaf', '128':'#fbfb95', '256':'#fafa7a', '512':'#f9f960',
                       '1024':'#f8f846', '2048':'#f7f72b', '4096':'', '8192':'', 
                       '16384':'', '32768':'', '65536':''}
        self.size_sq = 70
        self.last_matrices = []
        self.launch_game()
    
    def display_canv(self):
        self.canv = tk.Canvas(self, 
                              height=self.grid.size_grid*self.size_sq,
                              width=self.grid.size_grid*self.size_sq,
                              bg='white')
        for i in range(self.grid.size_grid):
            for j in range(self.grid.size_grid):
                self.canv.create_rectangle(j*self.size_sq, i*self.size_sq,
                                           (j+1)*self.size_sq, (i+1)*self.size_sq,
                                           width=1, fill=self.colors[str(int(self.grid.matrix[i][j]))])
                self.canv.create_text((j+0.5)*self.size_sq, (i+0.5)*self.size_sq,
                                      anchor=tk.CENTER,
                                      text=int(self.grid.matrix[i][j]))
        self.canv.pack()
        self.canv.focus_set()
        self.canv.bind("<Key>", self.clavier)
        
        self.button_back = tk.Button(self, text="Back", command=self.go_back)
        self.button_back.pack()

    
    def launch_game(self):
        self.grid.add_new()
        self.last_matrices.append(self.grid.matrix)
        self.display_canv()

    def each_turn(self, direction):
        print(self.grid.matrix)
        # print('Merge')
        self.grid.matrix = self.grid.merge_all(direction)
        # print(matrix)
        # print('Move')
        self.grid.move_all(direction)
        # print(matrix)
        # print('Add')
        self.grid.add_new()
        if self.grid.is_lost():
            self.status = "Perdu"
        if len(self.last_matrices)<100:
            self.last_matrices.append(self.grid.matrix)
        else:
            self.last_matrices = self.last_matrices[1:]
            self.last_matrices.append(self.grid.matrix)
        return self.grid.matrix

    def clavier(self, event):
        touche = event.keysym
        if touche=='Up':
            direction = 'U'
        if touche=='Down':
            direction = 'D'
        if touche=='Right':
            direction = 'R'
        if touche=='Left':
            direction = 'L'
        self.grid.matrix = self.each_turn(direction)
        if self.status == "Perdu":
            self.destroy()
        else:
            self.canv.destroy()
            self.button_back.destroy()
            self.display_canv()
    
    def go_back(self):
        if len(self.last_matrices) > 0:
            print('We go back')
            self.last_matrices = self.last_matrices[:-1]
            self.grid.matrix = self.last_matrices[-1]
            self.canv.destroy()
            self.button_back.destroy()
            self.display_canv()


class Grid(object):

    def __init__(self, size_grid=4, proba_4=0.15):
        self.matrix = np.zeros((size_grid, size_grid))
        self.size_grid = size_grid
        self.proba_4 = proba_4

    def add_new(self):
        """
        Add a new value in free case
        It modifies the matrix and returns it
        """
        possible_new = []
        for i in range(self.size_grid):
            for j in range(self.size_grid):
                if self.matrix[i][j] == 0:
                    possible_new.append((i,j))
        if len(possible_new) != 0:
            new_pos = rd.choice(possible_new)
            new_i = new_pos[0]
            new_j = new_pos[1]
            value = np.random.binomial(1, self.proba_4)
            if value == 0:
                new_val = 2
            else:
                new_val = 4
            self.matrix[new_i][new_j] = new_val
            return self.matrix

    def merge_all(self, direction):
        """
        Merge cases that has to be merge in the direction
        Doesn't change the initial matrix but return the modified matrix
        """
        matrix = np.copy(self.matrix)
        if direction == "R":
            for k in range(self.size_grid): # for all lines
                line = matrix[k, :]
                new_line = np.copy(line)
                last_num = None
                last_l = None
                for l in range(self.size_grid):
                    if line[self.size_grid-1-l] != 0:
                        if last_num == line[self.size_grid-1-l]:
                            new_line[self.size_grid-1-l] *= 2
                            new_line[self.size_grid-1-last_l] = 0
                            last_num = None
                        else :
                            last_num = line[self.size_grid-1-l]
                            last_l = l
                matrix[k, :] = new_line
        if direction == "L":
            for k in range(self.size_grid): # for all lines
                line = matrix[k, :]
                new_line = np.copy(line)
                last_num = None
                last_l = None
                for l in range(self.size_grid):
                    if line[l] != 0:
                        if last_num == line[l]:
                            new_line[l] *= 2
                            new_line[last_l] = 0
                            last_num = None
                        else :
                            last_num = line[l]
                            last_l = l
                matrix[k, :] = new_line
        if direction == "U":
            for k in range(self.size_grid): # for all lines
                col = matrix[:, k]
                new_col = np.copy(col)
                last_num = None
                last_l = None
                for l in range(self.size_grid):
                    if col[l] != 0:
                        if last_num == col[l]:
                            new_col[l] *= 2
                            new_col[last_l] = 0
                            last_num = None
                        else :
                            last_num = col[l]
                            last_l = l
                matrix[:, k] = new_col
        if direction == "D":
            for k in range(self.size_grid): # for all lines
                col = matrix[:, k]
                new_col = np.copy(col)
                last_num = None
                last_l = None
                for l in range(self.size_grid):
                    if col[self.size_grid-1-l] != 0:
                        if last_num == col[self.size_grid-1-l]:
                            new_col[self.size_grid-1-l] *= 2
                            new_col[self.size_grid-1-last_l] = 0
                            last_num = None
                        else :
                            last_num = col[self.size_grid-1-l]
                            last_l = l
                matrix[:, k] = new_col
        return matrix

    def move_all(self, direction):
        """
        Move all cases in the direction "R" "L" "U" or "D" 
        It modifies the matrix and returns it
        """
        if direction == "R":
            print("Bouge à droite")
            for k in range(self.size_grid): # for all lines
                line = self.matrix[k, :]
                new_line = np.zeros(self.size_grid)
                if (line==np.zeros(self.size_grid)).all():
                    pass
                else :
                    nbr = 0
                    for l in range(self.size_grid):
                        if line[self.size_grid-1-l] != 0:
                            new_line[self.size_grid-1-nbr] = line[self.size_grid-1-l]
                            nbr += 1
                self.matrix[k, :] = new_line
        if direction == "L":
            print("Bouge à gauche")
            for k in range(self.size_grid): # for all lines
                line = self.matrix[k, :]
                new_line = np.zeros(self.size_grid)
                if (line==np.zeros(self.size_grid)).all():
                    pass
                else :
                    nbr = 0
                    for l in range(self.size_grid):
                        if line[l] != 0:
                            new_line[nbr] = line[l]
                            nbr += 1
                self.matrix[k, :] = new_line
        if direction == "U":
            print("Bouge en haut")
            for k in range(self.size_grid): # for all columns
                col = self.matrix[:, k]
                new_col = np.zeros(self.size_grid)
                if (col==np.zeros(self.size_grid)).all():
                    pass
                else :
                    nbr = 0
                    for l in range(self.size_grid):
                        if col[l] != 0:
                            new_col[nbr] = col[l]
                            nbr += 1
                self.matrix[:, k] = new_col
        if direction == "D":
            print("Bouge en bas")
            for k in range(self.size_grid): # for all columns
                col = self.matrix[:, k]
                new_col = np.zeros(self.size_grid)
                if (col==np.zeros(self.size_grid)).all():
                    pass
                else :
                    nbr = 0
                    for l in range(self.size_grid):
                        if col[self.size_grid-1-l] != 0:
                            new_col[self.size_grid-1-nbr] = col[self.size_grid-1-l]
                            nbr += 1
                self.matrix[:, k] = new_col
        return self.matrix

    def is_lost(self):
        if (self.merge_all('R') == self.matrix).all() and \
           (self.merge_all('L') == self.matrix).all() and \
           (self.merge_all('D') == self.matrix).all() and \
           (self.merge_all('U') == self.matrix).all():
            no_zeros = True
            for i in range(self.size_grid):
                for j in range(self.size_grid):
                    if self.matrix[i][j] == 0:
                        no_zeros = False
                        break
            if no_zeros == True:
                return True
        return False




def main():
    game = Game(size_grid=4, proba_4=0.15)
    game.mainloop()
    
    print("C'est fini !")
    print(game.grid.matrix)


if __name__ == "__main__":
    main()

